package domain

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"testing"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	"github.com/jmoiron/sqlx"
)

type DBTestFixtures struct {
	DB     *sqlx.DB
	Logger *log.Logger
}

type options struct {
	PgHost        string `env:"PG_HOST"`
	PgPort        int    `env:"PG_PORT"`
	PgDatabase    string `env:"PG_DATABASE"`
	PgUser        string `env:"PG_USER"`
	PgPassword    string `env:"PG_PASSWORD"`
	MigrationsDir string `env:"MIGRATIONS_DIR"`
}

func NewTestFixtures(t *testing.T) (*DBTestFixtures, error) {
	f := &DBTestFixtures{}

	port, _ := strconv.Atoi(os.Getenv("PG_PORT"))

	opts := options{
		PgHost:        os.Getenv("PG_HOST"),
		PgPort:        port,
		PgDatabase:    os.Getenv("PG_DATABASE"),
		PgUser:        os.Getenv("PG_USER"),
		PgPassword:    os.Getenv("PG_PASSWORD"),
		MigrationsDir: os.Getenv("MIGRATIONS_DIR"),
	}

	f.Logger = log.New(os.Stdout, "LOG\t", log.Ldate|log.Ltime|log.Lshortfile)

	// Set up DB connection
	dsn := fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=disable", opts.PgHost, opts.PgPort, opts.PgUser, opts.PgDatabase, opts.PgPassword)

	fmt.Println("Connecting to DB: " + dsn)
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		return f, fmt.Errorf("unable to connect to DB: %v", err)
	}
	f.DB = db

	// Run DB migrations
	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		return nil, fmt.Errorf("unable to create DB driver for migrations: %v", err)
	}

	m, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("file://%s", opts.MigrationsDir), "postgres", driver)
	if err != nil {
		return nil, fmt.Errorf("unable to create migrator: %v", err)
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		return nil, fmt.Errorf("unable to run DB migrations: %v", err)
	}

	t.Cleanup(func() {
		err = m.Down()
		if err != nil {
			t.Errorf("unable to run down migrations in cleanup: %v", err)
		}

		err := db.Close()
		if err != nil {
			t.Errorf("unable to close DB in cleanup: %v", err)
		}
	})

	return f, nil
}
