package task_test

import (
	"context"
	"testing"

	"go.opencensus.io/trace"

	"gitlab.com/jcorry/go-db-test/internal/domain"
	"gitlab.com/jcorry/go-db-test/internal/domain/task"
)

func TestPGStore_List(t *testing.T) {
	if testing.Short() {
		t.Skipf("skipping DB integration test: %v", t.Name())
	}
	f, err := domain.NewTestFixtures(t)
	if err != nil {
		t.Fatal(err)
	}

	s := task.NewPGStore(f.DB, f.Logger)

	tsk := &domain.Task{
		Title:       "test",
		Description: "test",
	}

	ctx := context.TODO()

	ctx, span := trace.StartSpan(ctx, "test.Task.List")
	defer span.End()

	err = s.Create(ctx, tsk)
	if err != nil {
		t.Errorf("error creating task: %v", err)
	}

	list, err := s.List(ctx)

	if err != nil {
		t.Errorf("unexpected err: %v", err)
	}

	if len(list) != 1 {
		t.Error("unexpected list length")
	}
}
