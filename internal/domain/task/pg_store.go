package task

import (
	"context"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/jcorry/go-db-test/internal/domain"
	"go.opencensus.io/trace"
)

// PGStore is the struct on which DB store interaction functions can exist
type PGStore struct {
	db     *sqlx.DB
	logger *log.Logger
}

// NewPGStore creates and returns a new PGStore
func NewPGStore(db *sqlx.DB, logger *log.Logger) *PGStore {
	return &PGStore{
		db:     db,
		logger: logger,
	}
}

// List retrieves a list of Tasks from the DB
func (s *PGStore) List(ctx context.Context) ([]domain.Task, error) {
	ctx, span := trace.StartSpan(ctx, "internal.task.List")
	defer span.End()
	var tasks []domain.Task

	const q = `SELECT * FROM task ORDER BY created_at DESC`

	if err := s.db.SelectContext(ctx, &tasks, q); err != nil {
		return nil, fmt.Errorf("selecting products: %v", err)
	}

	return tasks, nil
}

// Read returns a single Task identified by ID
func (s *PGStore) Read(ctx context.Context, uuid string) (*domain.Task, error) {
	ctx, span := trace.StartSpan(ctx, "internal.task.List")
	defer span.End()

	const q = `SELECT * FROM task WHERE uuid = $1`

	var t domain.Task
	row := s.db.QueryRowContext(ctx, q, uuid)
	if err := row.Scan(&t.UUID, &t.Title, &t.Description, &t.Created, &t.Updated); err != nil {
		return nil, err
	}

	return &t, nil
}

// Create creates a new Task in to store in the DB
func (s *PGStore) Create(ctx context.Context, t *domain.Task) error {
	ctx, span := trace.StartSpan(ctx, "internal.task.Create")
	defer span.End()

	const q = `INSERT INTO task (title, description) VALUES ($1, $2) RETURNING uuid`

	var uuid string

	row := s.db.QueryRowContext(ctx, q, t.Title, t.Description)
	err := row.Scan(&uuid)
	if err != nil {
		return err
	}

	t.UUID = uuid

	// get the timestamps
	row = s.db.QueryRowContext(ctx, `SELECT created_at, updated_at FROM task WHERE uuid = $1`, t.UUID)
	err = row.Scan(&t.Created, &t.Updated)
	if err != nil {
		return err
	}

	return nil
}

// Delete removes a Task, identified by ID, from the DB store
func (s *PGStore) Delete(id int64) error {
	return nil
}
