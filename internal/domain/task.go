package domain

import (
	"context"
	"time"
)

// TaskStore is a storage repository for Task types
//go:generate counterfeiter . TaskStore
type TaskStore interface {
	TaskReader
	TaskWriter
}

// TaskReader is a DB Reader interface
type TaskReader interface {
	List(ctx context.Context) ([]Task, error)
	Read(ctx context.Context, uuid string) (*Task, error)
}

// TaskWriter is a DB Writer interface
type TaskWriter interface {
	Create(ctx context.Context, t *Task) error
	Delete(id int64) error
}

// Task is the struct that represents a Task
type Task struct {
	UUID        string    `json:"uuid" db:"uuid"`
	Title       string    `json:"title" db:"title"`
	Description string    `json:"description" db:"description"`
	Created     time.Time `json:"created" db:"created_at"`
	Updated     time.Time `json:"updated" db:"updated_at"`
}
