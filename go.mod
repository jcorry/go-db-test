module gitlab.com/jcorry/go-db-test

go 1.14

require (
	github.com/dimfeld/httptreemux/v5 v5.0.2
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/golang-migrate/migrate/v4 v4.10.0
	github.com/google/uuid v1.1.1
	github.com/jessevdk/go-flags v1.4.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.9.1
	go.opencensus.io v0.22.3
	gopkg.in/go-playground/validator.v9 v9.31.0
)
