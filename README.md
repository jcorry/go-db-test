[![codecov](https://codecov.io/gl/jcorry/go-db-test/branch/master/graph/badge.svg)](https://codecov.io/gl/jcorry/go-db-test)

# go-db-test

Simple Go http API demonstrating testing methods

## Docker
Docker is used to run the Postgres instances used by the app.
	- `db` service is the Postgres server used by the application when it runs.
	- `db-test` is the Postgres server that the tests use

## DB Migrations

The application runs the DB migrations in the `migrations` directory on each run.

## Testing

The whole point of the application is to demonstrate different testing methods for DB adjacent code. Specifically,
I wanted to demonstrate:

- Using a test database that is a close copy of the production database to execute queries on. By "close copy" I mean
it's the same database (Postgres) at the same version (latest) with the same schema (managed by `./migrations`).

- Using SQL Mock to produce errors of varying types. It's not always possible

- Using SQL-Mock to produce rows - This is a useful alternative to standing up a replica database. I used to rely
entirely on this approach. I realized at some point that this fails to test/verify the behavior my app is relying on;
that I can query the database and get data back. I realized that the query/schema is where problems usually actually happen,
and since have been pretty convinced that good tests involve actually transacting with a database.

- Using counterfeiter (https://github.com/maxbrunsfeld/counterfeiter) to produce interface mocks which can record function call params, call counts and return whatever
values you specify. This is great for testing handlers or other code that calls my database store code. All I need in
those tests are to give my functions an interface compliant value that will behave the way the DB store code would. It's
a test of the handler...not the DB store code.