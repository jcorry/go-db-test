gen:
	go generate -mod vendor ./...

lint: ; golangci-lint run --config .golangci-lint.yml
