-- add uuid extension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- add set timestamp func for triggers
CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- create the task table
CREATE TABLE task (
    uuid UUID NOT NULL DEFAULT uuid_generate_v1(),
        CONSTRAINT pkey_task PRIMARY KEY (uuid),
    title VARCHAR(255) NOT NULL,
    description TEXT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

-- create the trigger to update the updated_at timestamp
CREATE TRIGGER set_timestamp
BEFORE UPDATE ON task
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();