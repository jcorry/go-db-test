package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jessevdk/go-flags"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/jcorry/go-db-test/cmd/http-api/internal/handlers"
)

type options struct {
	Postgres struct {
		Host     string `long:"postgres-host" env:"POSTGRES_HOST" description:"The Postgres DB hostname." required:"true"`
		Port     string `long:"postgres-port" env:"POSTGRES_PORT" description:"The Postgres DB port." required:"true"`
		User     string `long:"postgres-user" env:"POSTGRES_USER" description:"The Postgres DB username." required:"true"`
		Password string `long:"postgres-password" env:"POSTGRES_PASSWORD" description:"The Postgres DB password." required:"true"`
		Database string `long:"postgres-database" env:"POSTGRES_DATABASE" description:"The Postgres DB database." required:"true"`
	}
	MigrationsDir string `long:"migrations-dir" env:"MIGRATIONS_DIR" description:"The directory containing the DB migrations" required:"true"`
	APIHost       string `long:"api-host" env:"API_HOST" description:"The host address and port for the API to run on"`
	DebugHost     string `long:"debug-host" env:"DEBUG_HOST" description:"The host address and port for the debugger to run on"`
}

func main() {
	if err := run(); err != nil {
		log.Println("error :", err)
		os.Exit(1)
	}
}

func run() error {
	// Parse startup options
	var opts options
	_, err := flags.Parse(&opts)
	if err != nil {
		return fmt.Errorf("unable to parse options: %v", err)
	}

	// Set up a logger
	logger := log.New(os.Stdout, "LOG\t", log.Ldate|log.Ltime|log.Lshortfile)

	// Set up DB connection
	dsn := fmt.Sprintf("user=%s dbname=%s password=%s sslmode=disable", opts.Postgres.User, opts.Postgres.Database, opts.Postgres.Password)
	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		return fmt.Errorf("unable to connect to DB: %v", err)
	}
	defer func() {
		db.Close()
	}()

	// Run DB migrations
	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		return fmt.Errorf("unable to create DB driver for migrations: %v", err)
	}
	m, err := migrate.NewWithDatabaseInstance(
		fmt.Sprintf("file://%s", opts.MigrationsDir),
		"postgres", driver)
	if err != nil {
		return fmt.Errorf("unable to create migrator: %v", err)
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		return fmt.Errorf("unable to run DB migrations: %v", err)
	}

	// =========================================================================
	// Start Debug Service
	//
	// /debug/pprof - Added to the default mux by importing the net/http/pprof package.
	// /debug/vars - Added to the default mux by importing the expvar package.
	//
	// Not concerned with shutting this down when the application is shutdown.

	log.Println("main : Started : Initializing debugging support")

	go func() {
		log.Printf("main : Debug Listening %s", opts.DebugHost)
		log.Printf("main : Debug Listener closed : %v", http.ListenAndServe(opts.DebugHost, http.DefaultServeMux))
	}()

	// Set up the http handlers and router
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	api := http.Server{
		Addr:         opts.APIHost,
		Handler:      handlers.API(shutdown, logger, db),
		ReadTimeout:  time.Second * 30,
		WriteTimeout: time.Second * 30,
	}

	serverErrors := make(chan error, 1)
	// Serve it up
	go func() {
		log.Println(fmt.Sprintf("main: API listening on %s", api.Addr))
		serverErrors <- api.ListenAndServe()
	}()

	// Shut it down...

	select {
	case err := <-serverErrors:
		return errors.Wrap(err, "server error")

	case sig := <-shutdown:
		log.Printf("main : %v : Start shutdown", sig)

		// Give outstanding requests a deadline for completion.
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()

		// Asking listener to shutdown and load shed.
		err := api.Shutdown(ctx)
		if err != nil {
			log.Printf("main : Graceful shutdown did not complete in %v : %v", time.Second*10, err)
			err = api.Close()
		}

		// Log the status of this shutdown.
		switch {
		case sig == syscall.SIGSTOP:
			return errors.New("integrity issue caused shutdown")
		case err != nil:
			return errors.Wrap(err, "could not stop server gracefully")
		}
	}

	return nil
}
