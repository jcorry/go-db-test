package handlers

import (
	"log"
	"net/http"
	"os"

	"github.com/jmoiron/sqlx"
	"gitlab.com/jcorry/go-db-test/cmd/http-api/internal/middleware"
	"gitlab.com/jcorry/go-db-test/internal/domain/task"
	"gitlab.com/jcorry/go-db-test/internal/platform/web"
)

// API returns an http.Handler with routes defined
func API(shutdown chan os.Signal, logger *log.Logger, db *sqlx.DB) http.Handler {
	app := web.NewApp(shutdown, middleware.Logger(logger), middleware.Errors(logger), middleware.Metrics())

	taskStore := task.NewPGStore(db, logger)

	t := NewTask(taskStore)

	app.Handle(http.MethodGet, "/v1/tasks", t.List)
	app.Handle(http.MethodPost, "/v1/tasks", t.Create)
	app.Handle(http.MethodGet, "/v1/tasks/:uuid", t.Read)

	return app
}
