package handlers

import (
	"context"
	"database/sql"
	"net/http"

	"go.opencensus.io/trace"

	"gitlab.com/jcorry/go-db-test/internal/platform/web"

	"gitlab.com/jcorry/go-db-test/internal/domain"
)

// Task is the struct on which the task handling endpoints rest
type Task struct {
	Store domain.TaskStore
}

// NewTask returns a new Task handler
func NewTask(s domain.TaskStore) Task {
	return Task{Store: s}
}

// List retrieves the list of tasks and returns
func (t *Task) List(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Task.List")
	defer span.End()

	tasks, err := t.Store.List(ctx)
	if err != nil {
		return web.RespondError(ctx, w, err)
	}

	return web.Respond(ctx, w, tasks, http.StatusOK)
}

// Read retrieves a single Task from the DB and returns
func (t *Task) Read(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Task.List")
	defer span.End()

	task, err := t.Store.Read(ctx, params["uuid"])

	if err != nil {
		if err == sql.ErrNoRows {
			return web.RespondError(ctx, w, &web.Error{Err: err, Status: http.StatusNotFound})
		}

		return web.RespondError(ctx, w, err)
	}

	return web.Respond(ctx, w, task, http.StatusOK)
}

// Create adds a new task to the Task collection
func (t *Task) Create(ctx context.Context, w http.ResponseWriter, r *http.Request, params map[string]string) error {
	ctx, span := trace.StartSpan(ctx, "handlers.Task.Create")
	defer span.End()

	var rTask domain.Task
	if err := web.Decode(r, &rTask); err != nil {
		return web.RespondError(ctx, w, err)
	}

	if err := t.Store.Create(ctx, &rTask); err != nil {
		return web.RespondError(ctx, w, err)
	}

	return web.Respond(ctx, w, &rTask, http.StatusOK)
}
